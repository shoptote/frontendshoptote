export const actionTypes = {
  ADD_TOKEN: 'ADD_TOKEN',
  ADD_ID: 'ADD_ID',
  ADD_USERNAME: 'ADD_USERNAME',
};

export const actions = {
  addToken: token => ({ type: actionTypes.ADD_TOKEN, payload: token }),
  addId: id => ({ type: actionTypes.ADD_ID, payload: id }),
  addUsername: username => ({ type: actionTypes.ADD_USERNAME, payload: username }),
};

const initialState = {
  token: null,
  id: null,
  username: null,
};

export const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_TOKEN':
      state.token = action.payload;
      return state;
    case 'ADD_ID':
      state.id = action.payload;
      return state;
    case 'ADD_USERNAME':
      state.username = action.payload;
      return state;
    default:
      return state;
  }
};
