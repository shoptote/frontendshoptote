import React from 'react';
import NavBar from './Components/Common/NavBar'
import { Switch, Route, BrowserRouter } from 'react-router-dom';
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';
import './App.css';

import HomePage from './Components/HomePage/HomePage'
import SignUpModal from './Components/Common/SignUpModal'
import LoginModal from './Components/Common/LoginModal'
import ShoppingListPage from './Components/ShoppingListPage/ShoppingListPage';
import RecipeSearchBar from './Components/RecipeSearch/RecipeSearchBar';

function App() {
  return (
    <div className="App">
      <ToastContainer />
      <BrowserRouter>
        <NavBar />
        <Switch>
          <Route path="/" exact component={HomePage} />
          <Route path="/signup" exact component={SignUpModal} />
          <Route path="/login" exact component={LoginModal} />
          <Route path="/shoppinglist" exact component={ShoppingListPage} />
          <Route path="/recipe" exact component={RecipeSearchBar} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
