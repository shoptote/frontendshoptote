import React, { useState } from 'react';
import {Form, FormGroup, Label, Input, Row, Col, Button} from 'reactstrap'
import { toast } from 'react-toastify';

export default function SignUp(props){
    const [firstName,setFirstName] = useState('');
    const [lastName,setLastName] = useState('');
    const [username,setUsername] = useState('');
    const [password,setPassword] = useState('');
    const [admin, setAdmin] = useState(false);
    const [banned] = useState(false);

    const handleAdmin = () => {
        setAdmin(prevState => !prevState)
    }

    function handleOnClick(e){
        e.preventDefault();

        fetch('http://18.224.71.125:8080/user/register',{
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({
                username: username,
                password: password,
                firstName: firstName,
                lastName: lastName,
                admin: admin,
                banned: banned
            })
        })
        .then(response => {
            toast.success('Signed Up!')
            //this throws an error when not using the modal but it should still work
            props.toggle();
        })
        .catch(err => toast.error('Hmm Not Quite...'))
    }

    return(
        <Form>
            <Row form>
                <Col md={6}>
                    <FormGroup>
                        <Label>First Name</Label>
                        <Input type="text" 
                        name="firstName" 
                        id="firstNameID" 
                        placeholder="First Name" 
                        onChange={e => setFirstName(e.target.value)} 
                        value={firstName}/>
                    </FormGroup>
                </Col>
                <Col md={6}>
                    <FormGroup>
                        <Label>Last Name</Label>
                        <Input type="text" 
                        name="lastName" 
                        id="lastNameID" 
                        placeholder="Last Name" 
                        onChange={e => setLastName(e.target.value)} 
                        value={lastName}/>
                    </FormGroup>
                </Col>
            </Row>
            <FormGroup>
                <Label>Username</Label>
                <Input type="text" 
                name="username" 
                id="usernameID" 
                placeholder="Username" 
                onChange={e => setUsername(e.target.value)} 
                value={username}/>
            </FormGroup>
            <FormGroup>
                <Label>Password</Label>
                <Input type="password" 
                name="password" 
                id="passwordID" 
                placeholder="Password" 
                onChange={e => setPassword(e.target.value)} 
                value={password}/>
            </FormGroup>
            <Col md={12}>
                <FormGroup>
                    <Label check>
                        <Input 
                        type="checkbox"
                        name="admin"
                        id="admin" 
                        onClick={handleAdmin}/>
                        Admin
                    </Label>
                </FormGroup>
            </Col>
            <Button color="primary" type="submit" onClick={e => handleOnClick(e)}>
                Submit
            </Button>
        </Form>
    )
}