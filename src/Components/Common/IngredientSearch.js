import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';

import IngredientSearchResults from './IngredientSearchResults';

export default function IngredientSearch({ addItem }) {
    const [query, setQuery] = useState('');
    const [showResults, setShowResults] = useState(false);
    const [searchResults, setSearchResults] = useState([]);

    const token = useSelector(state => state.token);
    
    // Queries database and retreives items beginning with query
    const handleFind = () => {
        if (query.length) {
            fetch(`http://18.224.71.125:8080/ingredients/name/${query}`, {
                method: 'GET',
                headers: { 'Authorization': `Bearer ${token}` }
            })
            .then(response => response.json())
            .then(body => setSearchResults(body))
            .then(setShowResults(true));
        }
    }
    
    // Handles input change
    const handleChange = (event) => {
        setQuery(event.target.value);
    }

    // Closes modal
    const closeResults = () => {
        setShowResults(false);
    }

    return (
        <div>
            <div>
                <input type="text" value={query} onChange={handleChange}/>
                <button onClick={handleFind}>Find Item</button>
            </div>
            <div>
                <Modal isOpen={showResults}>
                    <ModalHeader toggle={closeResults} charCode="X">
                        Ingredients
                    </ModalHeader>
                    <ModalBody>
                        <IngredientSearchResults ingredients={searchResults} addItem={addItem}/>
                    </ModalBody>
                </Modal>
            </div>
        </div>
        
    );
}
