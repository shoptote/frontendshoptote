import React, { useState } from 'react';
import {Modal, ModalHeader, ModalBody} from 'reactstrap'
import Login from './Login'

export default function LoginModal(props){

    const [modal, setModal] = useState(true);
    const toggle = () => {
        setModal(prevState => !prevState);
        props.visi();
    }

    //Maybe add some form validation
    return(
        <div>
            <Modal isOpen={modal}>
                <ModalHeader toggle={toggle} charCode="X">
                    Login
                </ModalHeader>
                <ModalBody>
                    <Login toggle={toggle}/>
                </ModalBody>
            </Modal>
        </div>
    )
}
