import React from 'react';

import IngredientSearchResultsItem from './IngredientSearchResultsItem';

export default function IngredientSearchResults({ ingredients, addItem }) {
    return (
        <div>
            {ingredients.map(ingredient => {
                return < IngredientSearchResultsItem key={ingredient.id} ingredient={ingredient} addItem={addItem}/>
            })}
        </div>
    );
}
