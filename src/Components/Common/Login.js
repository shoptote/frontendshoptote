import React, { useState } from 'react';
import {Form, FormGroup, Label, Input, Button} from 'reactstrap'
import { toast } from 'react-toastify';
import { useDispatch } from 'react-redux'
import { actions } from '../../redux/ducks/index'

export default function Login(props){

    const [username,setUsername] = useState('');
    const [password,setPassword] = useState('');

    //Set variables for dispatching props to store
    const dispatch = useDispatch();

    //Allows the user to login and get a JWT back as proof
    function handleOnClick(e){
        e.preventDefault();

        //Set the header
        const JWTHeader = new Headers();
        JWTHeader.append('Content-Type', 'application/json');
        JWTHeader.append('Access-Control-Expose-Headers', 'authorization');

        //Touch the login endpoint
        fetch('http://18.224.71.125:8080/login',{
	        method: 'POST',
            headers: JWTHeader,
            body: JSON.stringify({
                username: username,
                password: password,
            })
        })
        .then(response => {
            //Get JWT from header and parse
            let str = response.headers.get('Authorization')
            let JWT = str.split("Bearer ")

            //Send JWT to redux store
            dispatch(actions.addToken(JWT[1]));

            //Set header to get user info using the JWT from the login endpoint
            const userHeader = new Headers();
            userHeader.append('Content-Type', 'application/json');
            userHeader.append('Access-Control-Expose-Headers', 'authorization');
            userHeader.append('Authorization', `Bearer ${JWT[1]}`);
    
            //Fetch user info
            fetch(`http://18.224.71.125:8080/user/userinfo/${username}`, {
                method: 'GET',
                headers: userHeader,
            })
            .then(response => response.json())
            .then(data => {
                //Add to redux storage
                dispatch(actions.addId(data.id));
                dispatch(actions.addUsername(data.username));
                toast.success("Logged In!")
            })
            .catch(err => toast.error('Hmm Not Quite...'))
            props.toggle()
        })
        .catch(err => toast.error('Hmm Not Quite...'))
    }

    return(
        <div>
            <Form>
                <FormGroup>
                    <Label>Username</Label>
                    <Input type="text" 
                    name="username" 
                    id="usernameID" 
                    placeholder="Username" 
                    onChange={e => setUsername(e.target.value)} 
                    value={username}/>
                </FormGroup>
                <FormGroup>
                    <Label>Password</Label>
                    <Input type="password" 
                    name="password" 
                    id="passwordID" 
                    placeholder="Password" 
                    onChange={e => setPassword(e.target.value)} 
                    value={password}/>
                </FormGroup>
                <Button color="primary" type="submit" onClick={e => handleOnClick(e)}>
                    Submit
                </Button>
            </Form>
        </div>
    )
}