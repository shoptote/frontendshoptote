import React, { useState } from 'react';
import {Modal, ModalHeader, ModalBody} from 'reactstrap'
import SignUp from './SignUp'

//Toast not working
export default function SignUpModal(props){

    const [modal, setModal] = useState(true);

    const toggle = () => {
        setModal(prevState => !prevState);
        props.visi();
    }
    
    //Maybe add some form validation
    return(
        <div>
            <Modal isOpen={modal}>
                <ModalHeader toggle={toggle} charCode="X">
                    Sign Up
                </ModalHeader>
                <ModalBody>
                    <SignUp toggle={toggle}/>
                </ModalBody>
            </Modal>
        </div>
    )
}