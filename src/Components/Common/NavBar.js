import React, { useState, useEffect } from 'react';
import { NavLink, useHistory } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import { actions } from '../../redux/ducks/index'
import {Container, Row, Col, Button, Dropdown, DropdownToggle, DropdownMenu, DropdownItem} from 'reactstrap';
import LoginModal from './LoginModal';
import SignUpModal from './SignUpModal';
import { toast } from 'react-toastify';

//Implement getting JWT from redux and checking it from there
//If JWT Exists then show dropdown, if not then show login
//Needs Link to Favorites and Shopping List
const NavBar = () => {

    const [dropdownOpen, setDropdownOpen] = useState(false);
    const [loginVisi, setLoginVisi] = useState(true);
    const [signUpVisi, setSignUpVisi] = useState(true);
    const [dropdownVisi, setDropdownVisi] = useState(false);
    const [loginModalVisi, setLoginModalVisi] = useState(false);
    const [signUpModalVisi, setSignUpModalVisi] = useState(false);

    //Allow for redirection and redux dispatch
    let history = useHistory();
    const dispatch = useDispatch();

    //Get JWT from Redux
    const token = useSelector(state => state.token);
    const username = useSelector(state => state.username);

    //Toggle showing and hiding dropdown items
    const toggle = () => setDropdownOpen(prevState => !prevState);

    function handleLogout(event){
        dispatch(actions.addToken(null));
        dispatch(actions.addId(null));
        dispatch(actions.addUsername(null));
        toast.success("Logged Out!")
    }

    const reDirectShop = () => {
        history.push('/shoppinglist');
    }

    //Checks for the first time if the user is logged in
    useEffect(() => {
        if(!token){
            setLoginVisi(true)
            setSignUpVisi(true)
            setDropdownVisi(false)
        }
        else{
            setLoginVisi(false)
            setSignUpVisi(false)
            setDropdownVisi(true)
        }    
    }, [token])

    //function that shows the dropdown menu
    function DropdownMenuButton(){
        return(
            <Dropdown isOpen={dropdownOpen} toggle={toggle}>
                <DropdownToggle caret>
                    {username}
                </DropdownToggle>
                <DropdownMenu>
                    <DropdownItem>Favorites</DropdownItem>
                    <DropdownItem divider />
                    <DropdownItem onClick={reDirectShop}>Shopping List</DropdownItem>
                    <DropdownItem divider />
                    <DropdownItem onClick={handleLogout}>Logout</DropdownItem>
                </DropdownMenu>
            </Dropdown>
        )
    }

    //function to show login button
    function LoginButton(){
        return(
            <Button color='info' type="submit" onClick={showLoginModal}>Login</Button>
        )
    }

    function showLoginModal(){
        setLoginModalVisi(prevState => !prevState)
    }

    //function to show signup button
    function SignUpButton(){
        return(
            <Button color='info' type="submit" onClick={showSignUpModal}>Sign Up</Button>
        )
    }

    function showSignUpModal(){
        setSignUpModalVisi(prevState => !prevState)
    }

    const navBarStyle={
        backgroundColor: "#83d6c1",
        padding: "5px"
    }

	return (
        <div>
            <Container fluid style={navBarStyle}>
                <Row>
                    <Col>
                        <nav className="float-left">
                            <NavLink to='/' exact>Home</NavLink>
                        </nav>
                    </Col>
                    <Col>
                        <div className="float-right">
                            {loginVisi ? <LoginButton/> : null}
                            {' '}
                            {signUpVisi ? <SignUpButton/> : null}
                            {dropdownVisi ? <DropdownMenuButton/> : null}
                        </div>
                    </Col>
                </Row>
            </Container>
            {loginModalVisi ? <LoginModal visi={showLoginModal}/> : null}
            {signUpModalVisi ? <SignUpModal visi={showSignUpModal}/> : null}
        </div>
	);
};

export default NavBar;
