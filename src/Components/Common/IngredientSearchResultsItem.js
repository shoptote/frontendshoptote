import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { Form, Col, FormGroup, Label, Input, Button } from 'reactstrap';

export default function IngredientSearchResultsItem({ ingredient, addItem }) {
    const userID = useSelector(state => state.id);
    
    const [item, setItem] = useState({
        user_id: userID,
        ingredient_id: ingredient.id,
        quantity: 1,
        unit: '',
        bought: false
    });

    const handleChange = (event) => {
        setItem({ ...item, [event.target.name]: event.target.value });
    }

    return (
        <Form>
            <Col md={6}>
                <FormGroup>
                    <Input
                        type="number"
                        name="quantity"
                        value={item.quantity}
                        onChange={handleChange}
                    />
                    <Input
                        type="text"
                        name="unit"
                        placeholder="unit"
                        value={item.unit}
                        onChange={handleChange}
                    />
                    <Label>{ingredient.name}</Label>
                    <div>
                        <Button color="info" 
                            type="submit" 
                            onClick={(e) => { 
                                e.preventDefault();
                                addItem(item);
                            }}
                        >
                            Add Item
                        </Button>
                    </div>
                </FormGroup>
            </Col>
        </Form>
    );
}
