import React, { useEffect, useState } from 'react';
import { useSelector} from 'react-redux'
import { Container, Row, Col, Button } from 'reactstrap';

import HomePageSignUp from './HomePageSignUp'
import HomePageRedirection from './HomePageRedirection'
import { useHistory } from 'react-router-dom';

export default function HomePage(){

    const [signUpVisi, setSignUpVisi] = useState(true)
    const [reDirectVisi, setReDirectVisi] = useState(false)

    const token = useSelector(state => state.token);
    const history = useHistory()

    //Checks for the first time if the user is logged in
    useEffect(() => {
        if(!token){
            setSignUpVisi(true)
            setReDirectVisi(false)
        }

        else{
            setSignUpVisi(false)
            setReDirectVisi(true)
        }    
    }, [token])

    const reDirectRecipe = () => history.push("/recipe")

    const myStyle1 = {
        backgroundColor: "#ebf3ef"
    }

    return(
        <div>
            <Container fluid style={myStyle1}>
                <Row>
                    <Col md={9}>
                        <h1>Shoptote</h1>
                        <Button type="submit" onClick={reDirectRecipe}>Recipe Search</Button>
                    </Col>
                    <Col md={3}>
                        {signUpVisi ? <HomePageSignUp/> : null}
                        {reDirectVisi ? <HomePageRedirection/> : null}
                    </Col>
                </Row>
            </Container>
        </div>
    )
}