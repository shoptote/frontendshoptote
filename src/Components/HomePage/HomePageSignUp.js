import React, { useState } from 'react';
import Login from '../Common/Login'
import SignUp from '../Common/SignUp'

//Add the functionality to actually sign up and login
export default function HomePageSignUp(){

    const [signUpVisi, setSignUpVisi] = useState(true);
    const [loginVisi, setLoginVisi] = useState(false);

    const SwitchLayout = () => {
        setSignUpVisi(prevState => !prevState);
        setLoginVisi(prevState => !prevState);
    }

    const cursor = {
        cursor: "pointer"
    }

    //Display Sign Up Form
    function SignUpForm(){
        return(
            <div>
                <h2>Sign Up</h2>
                <SignUp/>
                <h6 onClick={SwitchLayout} style={cursor}>or if you already have an account then Login</h6>
            </div>
        )
    }

    //Display Login Form
    function LoginForm(){
        return(
            <div>
                <h2>Login</h2>
                <Login/>
                <h6 onClick={SwitchLayout} style={cursor}>Go back to Sign Up</h6>
            </div>
        )
    }

    return(
        <div>
            {signUpVisi ? <SignUpForm/> : null}
            {loginVisi ? <LoginForm/> : null}
        </div>
    )
}