import React from 'react';
import { useHistory } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { Row, Col, Button } from 'reactstrap';

//Needs redirection
export default function HomePageRedirection(){

    //Allow for redirection
    let history = useHistory();
    const username = useSelector(state => state.username);

    const reDirectShop = () => history.push('/shoppinglist')
    const reDirectFav = () => history.push('/favorites')

    return(
        <div>
            <Row>
                <Col>
                    <h2>{`Welcome Back ${username}`}</h2>
                    <h3>Checkout your</h3>
                    <Button onClick={reDirectFav}>Favorites</Button>
                    <h3>Checkout your</h3>
                    <Button onClick={reDirectShop}>Shopping List</Button>
                </Col>
            </Row>
        </div>
    )
}