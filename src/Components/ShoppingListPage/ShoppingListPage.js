import React from 'react';
import { Container, Col, Row } from 'reactstrap';

import ShoppingList from './ShoppingList';

export default function ShoppingListPage() {
    return (
        <Container fluid>
            <Row>
                <Col>
                    <h3>Shopping List</h3>                    
                </Col>
            </Row>
            <Row>
                <Col>
                    <ShoppingList />
                </Col>
            </Row>
        </Container>
    );
}
