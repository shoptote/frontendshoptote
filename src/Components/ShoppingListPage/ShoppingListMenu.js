import React from 'react';

import IngredientSearch from '../Common/IngredientSearch';
import { useSelector } from 'react-redux';
import { Row, Col } from 'reactstrap';

export default function ShoppingListMenu({ list, setList }) {
    const token = useSelector(state => state.token);

    const handleClear = () => {
        list.forEach(item => {
            if (item.bought) {
                fetch(`http://18.224.71.125:8080/shoppingitems/deletebyid/${item.id}`, {
                    method: 'DELETE',
                    headers: { 'Authorization': `Bearer ${token}` }
                })
            }
        });

        const result = list.filter(item => !item.bought);
        setList(result);
    }

    
    const addItem = (item) => {
        fetch('http://18.224.71.125:8080/shoppingitems/add', {
            method: 'POST',
            headers: { 
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(item)
        })
        .then(setList([ ...list, item ]));
    }

    return (
        <div>
            <Row>
                <Col>
                    <IngredientSearch addItem={addItem} />
                </Col>                
            </Row>
            <Row>
                <Col>
                    <button onClick={handleClear}>Clear Checked Items</button>
                </Col>
            </Row>
        </div>
    );
}
