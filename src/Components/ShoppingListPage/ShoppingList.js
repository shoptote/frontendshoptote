import React, { useState, useEffect, useCallback } from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { Table } from 'reactstrap';

import ShoppingListItem from './ShoppingListItem';
import ShoppingListMenu from './ShoppingListMenu';


export default function ShoppingList() {
    const [check, setCheck] = useState(false);
    const [shoppingList, setShoppingList] = useState([]);

    const userID = useSelector(state => state.id);
    const token = useSelector(state => state.token);

    let history = useHistory();

    const _getShoppingItems = async(userID, token) => {
        let list = null;

        await fetch(`http://18.224.71.125:8080/shoppingitems/userid?userId=${userID}`, {
            method: 'GET',
            headers: { 'Authorization': `Bearer ${token}`}
        })
        .then(response => response.json())
        .then(body => list = body);
        
        return list;
    };

    // Grabs user's shopping items from the database
    const getShoppingItems = useCallback((userID, token) => {
        _getShoppingItems(userID, token)
        .then(list => setShoppingList(list));
    }, []);

    // Updates all items on the list to the bought or unbought status
    const onChange = () => {
        const bought = !check
        setCheck(bought);
        shoppingList.forEach((item) => {
                item.bought = bought;
                fetch(`http://18.224.71.125:8080/shoppingitems/items/${item.id}`, {
                method: 'PUT',
                headers: { 
                    'Authorization': `Bearer ${token}`,
                    'Content-Type':'application/json;charset=utf-8'
                },
                body: JSON.stringify(item)
            });
        });
    }

    useEffect(() => {
        if (userID && token) {
            getShoppingItems(userID, token);
        } else {
            history.push('/');
        }
    }, [getShoppingItems, userID, token, history])
    
    return (
        <div>
            <Table>
                <thead>
                    <tr>
                        <th><input type="checkbox" checked={check} onChange={onChange}/></th>
                        <th><p>Check All Items</p></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {shoppingList
                    .sort((a, b) => a.id - b.id)
                    .map((item) => {
                        return (
                            <ShoppingListItem 
                                key={item.id}
                                item={item}
                                list={shoppingList}
                                setList={setShoppingList}
                            />
                        );
                    })}
                </tbody>
            </Table>            
            <ShoppingListMenu list={shoppingList} setList={setShoppingList}/>
        </div>
    );
}
