import React, { useState, useEffect, useCallback } from 'react';
import { useSelector } from 'react-redux';

export default function ShoppingListItem({ item, list, setList }) {
    const [listItem, setListItem] = useState({
        id: 0,
        quantity: 1,
        unit: '',
        bought: false,
        user_id: 0
    });
    const [ingredient, setIngredient] = useState({});

    const token = useSelector(state => state.token);

    // Grabs ingredient from database
    const getIngredient = useCallback(() => {
        fetch(`http://18.224.71.125:8080/ingredients/${item.ingredient_id}`, {
            method: 'GET',
            headers: { 'Authorization': `Bearer ${token}` }
        })
        .then(response => response.json())
        .then(body => setIngredient(body))
    }, [item.ingredient_id, token]);
    
    // Updates item in database
    const updateItems = () => {
        fetch(`http://18.224.71.125:8080/shoppingitems/items/${item.id}`, {
            method: 'PUT',
            headers: { 
                'Authorization': `Bearer ${token}`,
                'Content-Type':'application/json;charset=utf-8'
            },
            body: JSON.stringify(item)
        });
    }

    // Deletes item from database
    const deleteIngredient = () => {
        const results = list.filter((listItem) => listItem.id !== item.id);
        fetch(`http://18.224.71.125:8080/shoppingitems/deletebyid/${item.id}`, {
            method: 'DELETE',
            headers: { 'Authorization': `Bearer ${token}` }
        })
        .then(setList(results));
    }

    // Handles input change for checkboxes
    const handleChange = () => {
        setListItem({ ...listItem, bought: !listItem.bought });
        item.bought = !item.bought;
        updateItems();
    }

    // Handles input change for text and number
    const handleInput = (event) => {
        setListItem({ ...listItem, [event.target.name]: event.target.value });
        item[event.target.name] = event.target.value;
        updateItems();
    }

    useEffect(() => {
        setListItem(item);
        getIngredient();
    }, [getIngredient, item])

    return (
        <tr>
            <td>
                <input 
                    type="checkbox"
                    name="bought"
                    checked={listItem.bought}
                    onChange={handleChange}
                />
            </td>
            <td>
                <input 
                    type="number"
                    name="quantity"
                    value={listItem.quantity}
                    onChange={handleInput} 
                />
            </td>
            <td>
                <input 
                    type="text"
                    name="unit"
                    value={listItem.unit}
                    onChange={handleInput}
                />
            </td>
            <td>
                <p>{ingredient.name}</p>                
            </td>
            <td>
                <input
                    type="button"
                    value="X"
                    onClick={deleteIngredient}
                />
            </td>
        </tr>
    );
}
