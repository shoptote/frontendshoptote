import React from 'react'
import { Table, Button } from 'reactstrap'

export default function RecipeList(props){
    return(
        <div>
            <Table>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Favorited</th>
                        <th>Shopping List</th>
                    </tr>
                </thead>
                <tbody>
                    {props.recipes.map((recipe) => {
                        return(
                            <tr key={recipe.apiId}>
                                <td>{recipe.apiId}</td>
                                <td>{recipe.name}</td>
                                <td>
                                    <Button>Add to Favorites</Button>
                                </td>
                                <td>
                                    <Button>Add to List</Button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </Table>
        </div>
    )
}