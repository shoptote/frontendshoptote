import React, { useState } from 'react'
import {Button, Form, Input, ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem, Container, Row, Col, Label, FormGroup } from 'reactstrap'
import { useSelector} from 'react-redux'
import { toast } from 'react-toastify';

import RecipeList from './RecipeList';
import RecipeListAPI from './RecipeListAPI';

export default function RecipeSearchBar(){

    //stores for userinput
    const [name, setName] = useState("");
    const [ingredients, setIngredients] = useState("");
    const [minCarbs, setMinCarbs] = useState(0);
    const [minFat, setMinFat] = useState(0);
    const [recipe, setRecipe] = useState("")
    const [cuisine, setCuisine] = useState("")

    //store for data that is in the response
    const [dataArray, setDataArray] = useState([])

    //store for showing and hiding elements
    const [searchVisi, setSearchVisi] = useState(true);
    const [nameVisi, setNameVisi] = useState(false);
    const [ingredientsVisi, setIngredientsVisi] = useState(false);
    const [minCarbsVisi, setMinCarbsVisi] = useState(false);
    const [recipeVisi, setRecipeVisi] = useState(false)
    const [recipeListVisi, setRecipeListVisi] = useState(false)
    const [recipeListAPIVisi, setRecipeListAPIVisi] = useState(false)

    //store for dropdown box attributes
    const [dropdownOpen, setOpen] = useState(false);
    const [dropDownName, setDropDownName] = useState("Search");

    //Get JWT from the redux store
    const token = useSelector(state => state.token);

    //toggle dropdownbox items
    const toggle = () => setOpen(!dropdownOpen);

    //change the name of the dropdownbox in addition to hiding and showing input fields
    function handleNameChange(e){
        setDropDownName(e.target.name)
        switch(e.target.name){
            case "Name":
                setSearchVisi(false)
                setNameVisi(true)
                setIngredientsVisi(false)
                setMinCarbsVisi(false)
                setRecipeVisi(false)
                break;
            case "Ingredients":
                setSearchVisi(false)
                setNameVisi(false)
                setIngredientsVisi(true)
                setMinCarbsVisi(false)
                setRecipeVisi(false)
                break;
            case "Min Carbs and Fat":
                setSearchVisi(false)
                setNameVisi(false)
                setIngredientsVisi(false)
                setMinCarbsVisi(true)
                setRecipeVisi(false)
                break;
            case "Recipe":
                setSearchVisi(false)
                setNameVisi(false)
                setIngredientsVisi(false)
                setMinCarbsVisi(false)
                setRecipeVisi(true)
                break;
            default:
                setSearchVisi(true)
                setNameVisi(false)
                setIngredientsVisi(false)
                setMinCarbsVisi(false)
                setRecipeVisi(false)
        }
    }

    //Show different tables based on what search is made because fields are different based on the search
    function handleTableVisi(){
        if(dropDownName === "Name" || dropDownName === "Ingredients"){
            setRecipeListVisi(true)
            setRecipeListAPIVisi(false)
        }
        else{
            setRecipeListVisi(false)
            setRecipeListAPIVisi(true)
        }
    }

    //handle contacting the endpoint based on the type of search
    function handleSubmit(e){
        e.preventDefault();

        const postHeader = new Headers();
        postHeader.append('Content-Type', 'application/json');
        postHeader.append('Authorization', `Bearer ${token}`)

        const getHeader = new Headers();
        getHeader.append('Authorization', `Bearer ${token}`)

        switch(dropDownName){
            case "Name":
                fetch(`http://18.224.71.125:8080/spoon/name`, {
                    method: 'POST',
                    headers: postHeader,
                    body: name
                })
                .then(response => response.json())
                .then(data => {
                    if(data[0].apiId){
                        setDataArray(data)
                        handleTableVisi()
                    }
                })
                .catch(err => toast.error("Can't get the data!"))
                break;

            case "Ingredients":
                let ingredientsParse = ingredients.split(",")
                console.log(ingredientsParse)

                fetch(`http://18.224.71.125:8080/spoon/ingredients`, {
                    method: 'POST',
                    headers: postHeader,
                    body: JSON.stringify(ingredientsParse)
                })
                .then(response => response.json())
                .then(data => {
                    if(data[0].apiId){
                        setDataArray(data)
                        handleTableVisi()
                    }
                })
                .catch(err => toast.error("Can't get the data!"))
                break;

            case "Min Carbs and Fat":
                fetch(`http://18.224.71.125:8080/spoon/recipes/bynutrients/?minCarbs=${minCarbs}&minFat=${minFat}`, {
                    method: 'GET',
                    headers: getHeader,
                })
                .then(response => response.json())
                .then(data => {
                    if(data[0].id){
                        setDataArray(data)
                        handleTableVisi()
                    }
                })
                .catch(err => toast.error("Can't get the data!"))
                break;
            case "Recipe":
                fetch(`http://18.224.71.125:8080/spoon/recipes/?query=${recipe}&cuisine=${cuisine}`, {
                    method: 'GET',
                    headers: getHeader,
                })
                .then(response => response.json())
                .then(data => {
                    if(data.results[0].id){
                    setDataArray(data.results)
                    handleTableVisi()
                    }
                })
                .catch(err => toast.error("Can't get the data!"))
                break;
            default:
        }
    }

    //Dropdown menu that lets you change between search types and handles the submit
    function DropDownBox(){
        return(
            <ButtonDropdown isOpen={dropdownOpen} toggle={toggle}>
                <Button onClick={e => handleSubmit(e)}>{dropDownName}</Button>
                <DropdownToggle caret/>
                <DropdownMenu>
                    <DropdownItem name="Name" onClick={e => handleNameChange(e)}>Name</DropdownItem>
                    <DropdownItem divider />
                    <DropdownItem name="Ingredients" onClick={e => handleNameChange(e)}>Ingredients</DropdownItem>
                    <DropdownItem divider />
                    <DropdownItem name="Min Carbs and Fat" onClick={e => handleNameChange(e)}>Min Carbs and Fat</DropdownItem>
                    <DropdownItem divider />
                    <DropdownItem name="Recipe" onClick={e => handleNameChange(e)}>Recipe</DropdownItem>
                </DropdownMenu>
            </ButtonDropdown>
        )
    }

    //This horrid piece of code conditionally renders elements
    return(
        <div>
            <Container>
                <Row>
                    <Col>
                        <h1>Recipe Search</h1>
                    </Col>
                </Row>
                <Row>
                    <Col md={3}>
                        <DropDownBox/>
                    </Col>
                    <Col md={9}>
                        {searchVisi ? 
                            <Form>
                                <Input type="text" 
                                    name="Search" 
                                    placeholder="Search"/>
                            </Form>
                        : null}
                        {nameVisi ?             
                            <Form onSubmit={e => handleSubmit(e)}>
                                <Input type="text" 
                                    name="Name" 
                                    placeholder="Name" 
                                    onChange={e => setName(e.target.value)} 
                                    value={name}/>
                            </Form> 
                        : null}
                        {ingredientsVisi ? 
                            <Form onSubmit={e => handleSubmit(e)}>
                                <Input type="text" 
                                    name="Ingredients" 
                                    placeholder="Ingredients separate by commas" 
                                    onChange={e => setIngredients(e.target.value)} 
                                    value={ingredients}/>
                            </Form>
                        : null}
                        {minCarbsVisi ? 
                            <Form onSubmit={e => handleSubmit(e)}>
                                <FormGroup row>
                                    <Col md={1}>
                                        <Label for="MinCarbs">Min Carbs</Label>                                    
                                    </Col>
                                    <Col md={5}>
                                        <Input type="text" 
                                                name="Min Carbs"
                                                id="MinCarbs"
                                                onChange={e => setMinCarbs(e.target.value)} 
                                                value={minCarbs}/>                                    
                                    </Col>
                                    <Col md={1}>
                                        <Label for="MinFat">Min Fat</Label>
                                    </Col>
                                    <Col md={5}>
                                        <Input type="text" 
                                            name="Min Fat"
                                            id="MinFat"
                                            onChange={e => setMinFat(e.target.value)} 
                                            value={minFat}/>
                                    </Col>
                                </FormGroup>
                            </Form>
                        : null}
                        {recipeVisi ? 
                            <Form onSubmit={e => handleSubmit(e)}>
                                <FormGroup row>
                                    <Col md={2}>
                                        <Label for="Recipe">Recipe</Label>                                    
                                    </Col>
                                    <Col md={4}>
                                        <Input type="text" 
                                                name="Recipe"
                                                id="Recipe"
                                                onChange={e => setRecipe(e.target.value)} 
                                                value={recipe}/>                                    
                                    </Col>
                                    <Col md={2}>
                                        <Label for="Cuisine">Cuisine</Label>
                                    </Col>
                                    <Col md={4}>
                                        <Input type="text" 
                                            name="Cuisine"
                                            id="Cuisine"
                                            onChange={e => setCuisine(e.target.value)} 
                                            value={cuisine}/>
                                    </Col>
                                </FormGroup>
                            </Form>
                        : null}
                    </Col>
                </Row>
                <Row>
                    <Col>
                        {recipeListVisi ? <RecipeList recipes={dataArray}/> : null}
                        {recipeListAPIVisi ? <RecipeListAPI recipes={dataArray}/> : null}
                    </Col>
                </Row>
            </Container>
        </div>
    )
}