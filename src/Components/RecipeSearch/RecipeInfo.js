import React, { useState } from 'react'
import { Modal, ModalHeader, ModalBody, Row, Col} from 'reactstrap'

export default function RecipeInfo(props){

    const [modal, setModal] = useState(true);
    const imgSrc = `https://spoonacular.com/recipeImages/${props.recipeInfo.id}-480x360.jpg`

    const toggle = () => {
        setModal(prevState => !prevState);
    }

    const style = {
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
    }

    return(
        <div>
            <Modal isOpen={modal}>
                <ModalHeader toggle={toggle} charCode="X">
                    {props.recipeInfo.title}
                </ModalHeader>
                <ModalBody>
                    <Row style={style}>
                        <Col>
                            <img src={imgSrc} alt={props.recipeInfo.title}/>
                            <h5>Servings: {props.recipeInfo.servings}</h5>
                            <h5>Ready In Minutes: {props.recipeInfo.readyInMinutes}</h5>
                            <a href={props.recipeInfo.sourceUrl}>More Info</a>
                        </Col>
                    </Row>
                </ModalBody>
            </Modal>
        </div>
    )
}