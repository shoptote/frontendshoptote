import React, { useState } from 'react'
import { Table, Button } from 'reactstrap'
import RecipeInfo from './RecipeInfo';

//maybe use id to prevent the cascading modals
export default function RecipeListAPI(props){

    const [modalVisi, setModal] = useState(false)
    

    const toggleModal = (e) => {
        e.preventDefault()
        setModal(prevState => !prevState)
        e.stopPropagation()
    }

    return(
        <div>
            <Table>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                    </tr>
                </thead>
                <tbody>
                    {props.recipes.map((recipe) => {
                        return(
                            <tr key={recipe.id}>
                                <td>{recipe.id}</td>
                                <td>
                                    <Button outline color="info" onClick={e =>toggleModal(e)}>{recipe.title}</Button>
                                    {modalVisi ? <RecipeInfo recipeInfo={recipe}/> : null}
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </Table>
        </div>
    )
}